package com.example.javad.tamrin_calculator;

import android.renderscript.Sampler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    TextView tv_result;
    int num;
    Button btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn0,btnPlus,btnMinus,btnDot,btnDiv,btnMul,btnEqual;
    Float tempall;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         btn1 = (Button) findViewById(R.id.btn1);
         btn2 = (Button) findViewById(R.id.btn2);
         btn3 = (Button) findViewById(R.id.btn3);
         btn4 = (Button) findViewById(R.id.btn4);
         btn5 = (Button) findViewById(R.id.btn5);
         btn6 = (Button) findViewById(R.id.btn6);
         btn7 = (Button) findViewById(R.id.btn7);
         btn8 = (Button) findViewById(R.id.btn8);
         btn9 = (Button) findViewById(R.id.btn9);
         btn0 = (Button) findViewById(R.id.btn0);
         btnPlus = (Button) findViewById(R.id.btnPlus);
         btnMinus = (Button) findViewById(R.id.btnMul);
         btnMul = (Button) findViewById(R.id.btnMinus);
         btnEqual = (Button) findViewById(R.id.btnEqual);
         btnDiv = (Button) findViewById(R.id.btnDiv);
         btnDot = (Button) findViewById(R.id.btnDot);

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btn0.setOnClickListener(this);
        btnPlus.setOnClickListener(this);
        btnMinus.setOnClickListener(this);
        btnMul.setOnClickListener(this);
        btnDiv.setOnClickListener(this);
        btnDot.setOnClickListener(this);
        btnEqual.setOnClickListener(this);
    }
/*
    protected void strstr (View view1 , View view2){
        num+= Integer.valueOf(view1.toString())+Integer.valueOf(view2.toString());
    }

    protected void strstr (View view1){
        num+= Integer.valueOf(view1.toString());
        //String str = String.valueOf(view1).toString() + String.valueOf(view2).toString();
    }
*/
    math1 m1 = new math1();

    @Override
    public void onClick(View v) {
        tv_result= (TextView) findViewById(R.id.tv_Result);
        Log.d("num", String.valueOf(num));

        switch ( v.getId())
        {
            case R.id.btn1:
                show("1");
                break;
            case R.id.btn2:
                show("2");
                break;
           case R.id.btn3:
               show("3");
                break;
            case R.id.btn4:
                show("4");
                break;
            case R.id.btn5:
                show("5");
                break;
            case R.id.btn6:
                show("6");
                break;
            case R.id.btn7:
                show("7");
                break;
            case R.id.btn8:
                show("8");
                break;
            case R.id.btn9:
                show("9");
                break;
            case R.id.btn0:
                show("0");
                break;
            case R.id.btnPlus:
                plusReady();
                break;
            case R.id.btnMinus:
                //m1.Minus();
                break;
            case R.id.btnMul:
                //m1.Mul();
                break;
            case R.id.btnDiv:
                //m1.Div();
                break;
            case R.id.btnDot:
                int cur = tv_result.getText().toString().length();
                if (cur<1) {
                    show("0.");
                    //btnDot.setEnabled(false);
                    btnDot.setClickable(false);

                }
                else
                {
                    show(".");
                    btnDot.setClickable(false);
                   // btnDot.setEnabled(false);
                }
                break;
            case R.id.btnEqual:
                //editText_result.setText("0");
                break;
        }

    }


  /*  private void check (String nstar , boolean flag){
        String current = tv_result.getText().toString();
        if (current.equals(".")){
            tv_result.setText("0.");
            flag=true;
        }else
        {
            tv_result.append(".");
            flag=true;
        }
    }
    */

    private void show (String nstar){
        String current = tv_result.getText().toString();
        if (current.equals("0")){
            tv_result.setText(nstar);
        }else
        {
            tv_result.append(nstar);
        }
    }


    private void plusReady(){
        Float temp1 = Float.parseFloat(tv_result.getText().toString());
        tv_result.setText("0");
        Log.d("temp1",String.valueOf(temp1));
        //tempall+= temp1;
    }

    private void plusReady(Float n2){
        Float temp1 = Float.parseFloat((tv_result.getText().toString()));
        tv_result.setText("0");
        Log.d("temp1",String.valueOf(temp1));
        tempall+= temp1 + n2;
    }

    private void Equal(String nstar){
        if (nstar.equals("+"))
        {
            Float temp1 = Float.parseFloat((tv_result.getText().toString()));
            if (temp1 != 0){
                plusReady(temp1);
            }else plusReady();
        }
    }
}
